clear all
clc

DHp=[   0        0  sym('t1') sym('d1'); 
     pi/2        0  sym('t2')        0 ; 
        0 sym('a2') sym('t3')        0 ;
        0 sym('a3')        0         0 ;];
   
 %Determining No of Links from DH Table
SizeDH = size(DHp);
n = SizeDH(1,1);

tdot = sym(zeros(1, n));
tddot = sym(zeros(1, n));
for i = 1:n
    tdot(i) = sym(sprintf('tdot%d', i)); %
    tddot(i) = sym(sprintf('tddot%d', i));
end

%T is a 4x4 Identity Matrix
Temp = eye(4);
 T= sym(zeros(4,4,n));
 Tn= sym(zeros(4,4,n));
 R= sym(zeros(3,3,n));
 P=sym(zeros(3,1,n));
for i=1:n
    %Calculating (i-1)T(i) transformation Matrix
    B= [                cos(DHp(i,3))                (-sin(DHp(i,3)))             0                  DHp(i,2);
        (sin(DHp(i,3))*cos(DHp(i,1)))   (cos(DHp(i,1))*cos(DHp(i,3))) -sin(DHp(i,1)) (sin(DHp(i,1))*DHp(i,4));
        (sin(DHp(i,1))*sin(DHp(i,3)))   (sin(DHp(i,1))*cos(DHp(i,3)))  cos(DHp(i,1)) (cos(DHp(i,1))*DHp(i,4));
                                   0                               0              0                        1];
    B = simplify(B);
    T(:,:,i)= B; %Transformation Matrices
    R(:,:,i)= B(1:3,1:3); %Rotation Matrices
    P(:,:,i)= B(1:3,4); %Position Vectors
    Temp = Temp * B; %Calculating Final Transformation Matrix
    Temp =simplify(Temp);
    Tn(:,:,i) = Temp;
end

%disp(Temp)

W=sym(zeros(3,n)); %Angular Velocity
w=sym(zeros(3,1));
V=sym(zeros(3,n)); %Linear Velocity
v=sym(zeros(3,1));
Vdot=sym(zeros(3,n)); %Linear Accelration
vdot=sym(zeros(3,1)); 
wdot=sym(zeros(3,1));
Wdot=sym(zeros(3,n)); %Angular Accelration

for i=1:n
    %% Linera Velocity
    v = (R(:,:,i).')*(v + cross(w,P(:,:,i)));
    v = simplify(v);
    V(:,i) = v;
    
    %% Linear Accelration
    vdot = (R(:,:,i).')*(cross(wdot,P(:,:,i))+ cross(w,cross(w,P(:,:,i))) + vdot);
    vdot = simplify(vdot);
    Vdot(:,i)= vdot;
    %% Angular Accelration
    wdot=((R(:,:,i).')* wdot)+ cross(((R(:,:,i).')* w),[0;0;tdot(i)])+[0;0;tddot(i)];
    wdot= simplify(wdot);
    Wdot(:,i)=wdot;
    
    %% Angular Velocity
    w = ((R(:,:,i).')* w) +[0;0;tdot(i)];
    w=simplify(w);
    %disp (w);
    W(:,i)= w;
end


f=[sym('Fx');sym('Fy');sym('Fz')];
t=sym(zeros(3,1));
F=sym(zeros(3,n+1));
N=sym(zeros(3,n+1));
F(:,n+1)=f;
N(:,n+1)=t;
for i=n:-1:1
    %% Force
    f = R(:,:,i)*f;
    f = simplify(f);
    F(:,i)=f;
    
    %%Torque
    t = (R(:,:,i)*t) + cross(P(:,:,i),f);
    t = simplify(t);
    N(:,i) = t;
end

Pc=[0 sym('a2') sym('a3'); 
    0        0         0 ;
    0        0         0 ];

Vcdot=sym(zeros(3,n-1));
for i=1:n-1
    Vcdot(:,i)= cross(Wdot(:,i),Pc(:,i))+ cross(W(:,i),cross(W(:,i),Pc(:,i)))+ Vdot(:,i);
    Vcdot=simplify(Vcdot);
end
