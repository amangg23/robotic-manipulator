clear all
clc

t= 0:0.1:50;
px = 0.4 + 0.2*sin(0.1*t);
py = 0.1* sin(0.1*t);
pz = 0.2 + 0.2*sin(0.1*t);
%% Constant kinmatic parameters of the robot

a2 = 0.3; d1 = 0.3; a3 = 0.6;

%initializing arrays for memory efficiency
th1 = zeros(length(t));
s3 =  zeros(length(t));
c3= zeros(length(t));
th3= zeros(length(t));
sa2= zeros(length(t));
ca2= zeros(length(t));
th2= zeros(length(t));
xn= zeros(length(t));
yn= zeros(length(t));
zn= zeros(length(t));
x= zeros(length(t),5);
y= zeros(length(t),5);
z= zeros(length(t),5);
k1=zeros(length(t));
k2=zeros(length(t));
r=zeros(length(t));
%% Inverse Kinematic model

for i = 1:length(t)
%theta1
th1(i) = atan2(py(i),px(i));

%theta3
c3(i)= (px(i)^2 + py(i)^2 +(pz(i)-d1)^2- a2^2 - a3^2)/(2*a2*a3);
s3(i)= sqrt(1-c3(i)^2);
th3(i)= atan2(s3(i),c3(i));

%theta2
k1(i)=(a3*c3(i))+a2;
k2(i)=(a3*s3(i));
r(i)=sqrt(k1(i)^2+k2(i)^2);
sa2(i) =(pz(i)-d1)/r(i);
ca2(i) = sqrt(1-sa2(i)^2);
th2(i) = atan2(sa2(i),-ca2(i))-atan2(k2(i)/r(i),k1(i)/r(i));
%% Forward Kinematic model

xn(i) = a2*cos(th1(i))*(cos(th2(i)+th3(i))+cos(th2(i)));
yn(i) = a2*sin(th1(i))*(cos(th2(i)+th3(i))+cos(th2(i)));
zn(i) = d1 + a2*sin(th2(i)+th3(i)) + a2*sin(th2(i));

x(i,:) = [0   0   0 a2*cos(th1(i))*cos(th2(i)) xn(i) ];
y(i,:) = [0   0   0 a2*sin(th1(i))*cos(th2(i)) yn(i) ];
z(i,:) = [0  d1  d1          d1+a2*sin(th2(i)) zn(i) ];

end

for i = 1:length(t)
    plot3(x(i,:),y(i,:),z(i,:),'ro-','LineWidth',5,...
                       'MarkerEdgeColor','r',...
                       'MarkerFaceColor','r',...
                       'MarkerSize',10)
    hold on;
    plot3(xn(1:i),yn(1:i),zn(1:i),'b-');
    axis([-0.6 0.6 -0.6 0.6 -0.6 0.6]);
    grid on
    pause(0.01);
    hold off;
end

%close all;

