clear all
clc


%% Constant kinmatic parameters of the robot

a2 = 0.4; d1 = 0.2; a3 = 0.2;

th1(1:41)=0;
th2=0:(pi/60):(2*pi/3);
th3=0:(-pi/60):(-2*pi/3);
%th3(1:41)=0;

xn=zeros(length(th1));
yn=zeros(length(th1));
zn=zeros(length(th1));
x=zeros(length(th1),5);
y=zeros(length(th1),5);
z=zeros(length(th1),5);


for i = 1:length(th1)
%% Forward Kinematic model

xn(i) = a2*cos(th1(i))*(cos(th2(i)+th3(i))+cos(th2(i)));
yn(i) = a2*sin(th1(i))*(cos(th2(i)+th3(i))+cos(th2(i)));
zn(i) = d1 + a2*sin(th2(i)+th3(i)) + a2*sin(th2(i));

x(i,:) = [0   0   0 a2*cos(th1(i))*cos(th2(i)) xn(i) ];
y(i,:) = [0   0   0 a2*sin(th1(i))*cos(th2(i)) yn(i) ];
z(i,:) = [0  d1  d1          d1+a2*sin(th2(i)) zn(i) ];

end

for i = 1:length(th1)
    plot3(x(i,:),y(i,:),z(i,:),'ro-','LineWidth',5,...
                       'MarkerEdgeColor','r',...
                       'MarkerFaceColor','r',...
                       'MarkerSize',10)
    hold on;
    plot3(xn(1:i),yn(1:i),zn(1:i),'b-');
    axis([-0.6 0.6 -0.6 0.6 -0.6 0.6]);
    grid on
    pause(0.1);
    hold off;
end

%close all;

