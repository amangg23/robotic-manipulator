%Clearing Memory and Screen
clear all
clc

%%
%DH Parameters Table alpha-a-theta-d in order
DHp=[   0        0  sym('t1') sym('d1'); 
     pi/2        0  sym('t2')        0 ; 
        0 sym('a2') sym('t3')        0 ;
        0 sym('a3')        0         0 ;];

%Determining No of Links from DH Table
SizeDH = size(DHp);
n = SizeDH(1,1);

%%
%T is a 4x4 Identity Matrix
T = eye(4);

for i=1:n
    %Calculating (i-1)T(i) Transformation Matrix
    B= [                cos(DHp(i,3))                (-sin(DHp(i,3)))             0                  DHp(i,2);
        (sin(DHp(i,3))*cos(DHp(i,1)))   (cos(DHp(i,1))*cos(DHp(i,3))) -sin(DHp(i,1)) (sin(DHp(i,1))*DHp(i,4));
        (sin(DHp(i,1))*sin(DHp(i,3)))   (sin(DHp(i,1))*cos(DHp(i,3)))  cos(DHp(i,1)) (cos(DHp(i,1))*DHp(i,4));
                                   0                               0              0                        1];
    B = simplify(B);
    %disp(B); %Show All Transformation Matrices
    T = T * B; %Calculating Final Transformation Matrix
    T =simplify(T);
end

%%
%Displaying Results
fprintf('\nForward Kinematics Solution is\n\nT-0-N =\n')
disp(T)

fprintf('\nn =\n')
disp(T(1:3,1))

fprintf('\ns =\n')
disp(T(1:3,2))

fprintf('\na =\n')
disp(T(1:3,3))

fprintf('\np =\n')
disp(T(1:3,4))

